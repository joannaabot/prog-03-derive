# Welcome to Framer

# This is all CoffeeScript. Learn here: http://framerjs.com/learn.html#coffeescript

# Drop an image on the preview screen to create an image layer, or use the generator to import assets from Sketch or Photoshop

imageLayer = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 3 Derive - Join-11.psd"
imageLayer1 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 3 Derive - Join-10.psd"
imageLayer2 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 3 Derive - Join-09.psd"
imageLayer3 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 3 Derive - Join-08.psd"
imageLayer4 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 3 Derive - Join-07.psd"
imageLayer5 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 3 Derive - Join-06.psd"
imageLayer6 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 3 Derive - Join-05.psd"
imageLayer7 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 3 Derive - Join-04.psd"
imageLayer8 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 3 Derive - Join-03.psd"
imageLayer9 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 3 Derive - Join-02.psd"
imageLayer10 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 3 Derive - Join-01.psd"

# Mapping clickable layers to buttons
timeButton = new Layer
	x: 220
	y: 490
	width: 80
	height: 50

timeButton.backgroundColor = utils.randomColor(0)

chosenNext = new Layer
	x: 230
	y: 510
	width: 80
	height: 50

chosenNext.backgroundColor = utils.randomColor(0)

adventureNext = new Layer
	x: 230
	y: 510
	width: 80
	height: 50

adventureNext.backgroundColor = utils.randomColor(0)

joannaButton = new Layer
	x: 10
	y: 100
	width: 60
	height: 80

joannaButton.backgroundColor = utils.randomColor(0)

transportNext = new Layer
	x: 230
	y: 510
	width: 80
	height: 50

transportNext.backgroundColor = utils.randomColor(0)

walkingButton = new Layer
	x: 10
	y: 90
	width: 70
	height: 50

walkingButton.backgroundColor = utils.randomColor(0)

statusNext = new Layer
	x: 230
	y: 510
	width: 80
	height: 50

statusNext.backgroundColor = utils.randomColor(0)

curiousButton = new Layer
	x: 10
	y: 110
	width: 70
	height: 50

curiousButton.backgroundColor = utils.randomColor(0)

currLocButton = new Layer
	x: 20
	y: 475
	width: 80
	height: 50

currLocButton.backgroundColor = utils.randomColor(0)

joinButton = new Layer
	x: 220
	y: 480
	width: 80
	height: 50

joinButton.backgroundColor = utils.randomColor(0)

# On a click, go to the next state
joinButton.on Events.Click, ->
	imageLayer10.destroy()
	joinButton.destroy()

currLocButton.on Events.Click, ->
	imageLayer9.destroy()
	currLocButton.destroy()
	
curiousButton.on Events.Click, ->
	imageLayer8.destroy()
	curiousButton.destroy()
	
statusNext.on Events.Click, ->
	imageLayer7.destroy()
	statusNext.destroy()
	
walkingButton.on Events.Click, ->
	imageLayer6.destroy()
	walkingButton.destroy()

transportNext.on Events.Click, ->
	imageLayer5.destroy()
	transportNext.destroy()

joannaButton.on Events.Click, ->
	imageLayer4.destroy()
	joannaButton.destroy()

adventureNext.on Events.Click, ->
	imageLayer3.destroy()
	adventureNext.destroy()

chosenNext.on Events.Click, ->
	imageLayer2.destroy()
	chosenNext.destroy()

timeButton.on Events.Click, ->
	imageLayer1.destroy()
	timeButton.destroy()
	



	



	



	

	

	


