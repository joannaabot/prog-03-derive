# Welcome to Framer

# Learn how to prototype: http://framerjs.com/learn
# Drop an image on the device, or import a design from Sketch or Photoshop

# Watch deck
JoinWatch11 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/JoinWatchImages/JoinWatch11.png"

JoinWatch10 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/JoinWatchImages/JoinWatch10.png"

JoinWatch9 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/JoinWatchImages/JoinWatch9.png"

JoinWatch8 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/JoinWatchImages/JoinWatch8.png"

JoinWatch7 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/JoinWatchImages/JoinWatch7.png"

JoinWatch6 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/JoinWatchImages/JoinWatch6.png"

JoinWatch5 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/JoinWatchImages/JoinWatch5.png"

JoinWatch4 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/JoinWatchImages/JoinWatch4.png"

JoinWatch3 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/JoinWatchImages/JoinWatch3.png"

JoinWatch2 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/JoinWatchImages/JoinWatch2.png"
	
JoinWatch1 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/JoinWatchImages/JoinWatch1.png"

# Make the watch faces draggable 
JoinWatch2.draggable.enabled = true
JoinWatch3.draggable.enabled = true
JoinWatch4.draggable.enabled = true
JoinWatch5.draggable.enabled = true
JoinWatch6.draggable.enabled = true
JoinWatch7.draggable.enabled = true
JoinWatch8.draggable.enabled = true
JoinWatch9.draggable.enabled = true
JoinWatch10.draggable.enabled = true
JoinWatch11.draggable.enabled = true

# On a click, start getting directions
JoinWatch1.on Events.Click, ->
	JoinWatch1.destroy()
	
# By dragging the screen, get the next step
JoinWatch2.on Events.TouchStart, ->
	# Set the dragging speed so the user can drag horizontally
	JoinWatch2.draggable.speedX = 1
	# Disable dragging speed so the user cannot drag vertically
	JoinWatch2.draggable.speedY = 0
	
JoinWatch3.on Events.TouchStart, ->
	JoinWatch3.draggable.speedX = 1
	JoinWatch3.draggable.speedY = 0
	
JoinWatch4.on Events.TouchStart, ->
	JoinWatch4.draggable.speedX = 1
	JoinWatch4.draggable.speedY = 0
	
JoinWatch5.on Events.TouchStart, ->
	JoinWatch5.draggable.speedX = 1
	JoinWatch5.draggable.speedY = 0
	
JoinWatch6.on Events.TouchStart, ->
	JoinWatch6.draggable.speedX = 1
	JoinWatch6.draggable.speedY = 0
	
JoinWatch7.on Events.TouchStart, ->
	JoinWatch7.draggable.speedX = 1
	JoinWatch7.draggable.speedY = 0
	
JoinWatch8.on Events.TouchStart, ->
	JoinWatch8.draggable.speedX = 1
	JoinWatch8.draggable.speedY = 0
	
JoinWatch9.on Events.TouchStart, ->
	JoinWatch9.draggable.speedX = 1
	JoinWatch9.draggable.speedY = 0
	
JoinWatch10.on Events.TouchStart, ->
	JoinWatch10.draggable.speedX = 1
	JoinWatch10.draggable.speedY = 0
	
JoinWatch11.on Events.TouchStart, ->
	JoinWatch11.draggable.speedX = 1
	JoinWatch11.draggable.speedY = 0