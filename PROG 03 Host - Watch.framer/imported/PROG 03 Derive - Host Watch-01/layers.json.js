window.__imported__ = window.__imported__ || {};
window.__imported__["PROG 03 Derive - Host Watch-01/layers.json.js"] = [
	{
		"id": 11,
		"name": "Layer 1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 350,
			"height": 398
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 1.png",
			"frame": {
				"x": 0,
				"y": 0,
				"width": 350,
				"height": 398
			}
		},
		"imageType": "png",
		"children": [
			{
				"id": 6,
				"name": "<Group>",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 350,
					"height": 398
				},
				"maskFrame": null,
				"image": {
					"path": "images/<Group>.png",
					"frame": {
						"x": 27,
						"y": 162,
						"width": 298,
						"height": 137
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "35726723"
			}
		],
		"modification": "1157147660"
	}
]