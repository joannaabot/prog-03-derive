window.__imported__ = window.__imported__ || {};
window.__imported__["PROG 03 Derive - Join Watch-01/layers.json.js"] = [
	{
		"id": 13,
		"name": "Layer 1",
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 350,
			"height": 398
		},
		"maskFrame": null,
		"image": {
			"path": "images/Layer 1.png",
			"frame": {
				"x": 0,
				"y": 2,
				"width": 350,
				"height": 396
			}
		},
		"imageType": "png",
		"children": [
			{
				"id": 9,
				"name": "<Group>",
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 350,
					"height": 398
				},
				"maskFrame": null,
				"image": {
					"path": "images/<Group>.png",
					"frame": {
						"x": 29,
						"y": 170,
						"width": 298,
						"height": 137
					}
				},
				"imageType": "png",
				"children": [
					
				],
				"modification": "1102456446"
			}
		],
		"modification": "370110756"
	}
]