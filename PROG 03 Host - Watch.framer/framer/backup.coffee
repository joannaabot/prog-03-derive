# This imports all the layers for "Derive Host Watch 01-01" into deriveHostWatch0101Layers7
#deriveHostWatch0101Layers7 = Framer.Importer.load "imported/Derive Host Watch 01-01"

# Welcome to Framer

# Learn how to prototype: http://framerjs.com/learn
# Drop an image on the device, or import a design from Sketch or Photoshop

# Watch deck
HostWatch11 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/HostWatchImages/HostWatch11.png"

HostWatch10 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/HostWatchImages/HostWatch10.png"

HostWatch9 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/HostWatchImages/HostWatch9.png"

HostWatch8 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/HostWatchImages/HostWatch8.png"

HostWatch7 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/HostWatchImages/HostWatch7.png"

HostWatch6 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/HostWatchImages/HostWatch6.png"

HostWatch5 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/HostWatchImages/HostWatch5.png"

HostWatch4 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/HostWatchImages/HostWatch4.png"

HostWatch3 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/HostWatchImages/HostWatch3.png"

HostWatch2 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/HostWatchImages/HostWatch2.png"
	
HostWatch1 = new Layer
	width: 320
	height: 364
	image:"/home/cc/cs160/fa14/class/cs160-am/Documents/HostWatchImages/HostWatch1.png"

# Make the watch faces draggable 
HostWatch2.draggable.enabled = true
HostWatch3.draggable.enabled = true
HostWatch4.draggable.enabled = true
HostWatch5.draggable.enabled = true
HostWatch6.draggable.enabled = true
HostWatch7.draggable.enabled = true
HostWatch8.draggable.enabled = true
HostWatch9.draggable.enabled = true
HostWatch10.draggable.enabled = true
HostWatch11.draggable.enabled = true

# On a click, start getting directions
HostWatch1.on Events.Click, ->
	HostWatch1.destroy()
	
# By dragging the screen, get the next step
HostWatch2.on Events.TouchStart, ->
	# Set the dragging speed so the user can drag horizontally
	HostWatch2.draggable.speedX = 1
	# Disable dragging speed so the user cannot drag vertically
	HostWatch2.draggable.speedY = 0
	
HostWatch3.on Events.TouchStart, ->
	HostWatch3.draggable.speedX = 1
	HostWatch3.draggable.speedY = 0
	
HostWatch4.on Events.TouchStart, ->
	HostWatch4.draggable.speedX = 1
	HostWatch4.draggable.speedY = 0
	
HostWatch5.on Events.TouchStart, ->
	HostWatch5.draggable.speedX = 1
	HostWatch5.draggable.speedY = 0
	
HostWatch6.on Events.TouchStart, ->
	HostWatch6.draggable.speedX = 1
	HostWatch6.draggable.speedY = 0
	
HostWatch7.on Events.TouchStart, ->
	HostWatch7.draggable.speedX = 1
	HostWatch7.draggable.speedY = 0
	
HostWatch8.on Events.TouchStart, ->
	HostWatch8.draggable.speedX = 1
	HostWatch8.draggable.speedY = 0
	
HostWatch9.on Events.TouchStart, ->
	HostWatch9.draggable.speedX = 1
	HostWatch9.draggable.speedY = 0
	
HostWatch10.on Events.TouchStart, ->
	HostWatch10.draggable.speedX = 1
	HostWatch10.draggable.speedY = 0
	
HostWatch11.on Events.TouchStart, ->
	HostWatch11.draggable.speedX = 1
	HostWatch11.draggable.speedY = 0

