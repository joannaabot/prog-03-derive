# Welcome to Framer

# This is all CoffeeScript. Learn here: http://framerjs.com/learn.html#coffeescript

# Drop an image on the preview screen to create an image layer, or use the generator to import assets from Sketch or Photoshop

# Phone screens for Host (main user)
imageLayer = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 03 Derive - Host-17.psd"
imageLayer1 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 03 Derive - Host-16.psd"
imageLayer2 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 03 Derive - Host-15.psd"
imageLayer3 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 03 Derive - Host-14.psd"
imageLayer4 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 03 Derive - Host-13.psd"
imageLayer5 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 03 Derive - Host-12.psd"
imageLayer6 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 03 Derive - Host-11.psd"
imageLayer7 = new Layer x:0, y:0, width:321, height:570, image:"images/PROG 03 Derive - Host-10.psd"
imageLayer8 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 03 Derive - Host-09.psd"
imageLayer9 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 03 Derive - Host-08.psd"
imageLayer10 = new Layer x:0, y:0, width:321, height:570, image:"images/PROG 03 Derive - Host-07.psd"
imageLayer11 = new Layer x:0, y:0, width:321, height:571, image:"images/PROG 03 Derive - Host-06.psd"
imageLayer12 = new Layer x:0, y:0, width:321, height:570, image:"images/PROG 03 Derive - Host-05.psd"
imageLayer13 = new Layer x:0, y:0, width:321, height:570, image:"images/PROG 03 Derive - Host-04.psd"
imageLayer14 = new Layer x:0, y:0, width:321, height:570, image:"images/PROG 03 Derive - Host-03.psd"
imageLayer15 = new Layer x:0, y:0, width:321, height:570, image:"images/PROG 03 Derive - Host-02.psd"
imageLayer16 = new Layer x:0, y:0, width:321, height:570, image:"images/PROG 03 Derive - Host-01.psd"

# Mapping clickable layers to buttons
verifyButton = new Layer
	x: 30
	y: 505
	width: 80
	height: 50

verifyButton.backgroundColor = utils.randomColor(0)

featureNext = new Layer
	x: 240
	y: 510
	width: 80
	height: 50

featureNext.backgroundColor = utils.randomColor(0)

touristyButton = new Layer
	x: 10
	y: 375
	width: 70
	height: 50

touristyButton.backgroundColor = utils.randomColor(0)

cityButton = new Layer
	x: 10
	y: 235
	width: 70
	height: 50

cityButton.backgroundColor = utils.randomColor(0)

parksButton = new Layer
	x: 10
	y: 160
	width: 70
	height: 50

parksButton.backgroundColor = utils.randomColor(0)

timeNext = new Layer
	x: 240
	y: 510
	width: 80
	height: 50

timeNext.backgroundColor = utils.randomColor(0)

timeButton = new Layer
	x: 10
	y: 310
	width: 70
	height: 50

timeButton.backgroundColor = utils.randomColor(0)

martyDocNext = new Layer
	x: 240
	y: 510
	width: 80
	height: 50

martyDocNext.backgroundColor = utils.randomColor(0)

docButton = new Layer
	x: 10
	y: 160
	width: 70
	height: 50

docButton.backgroundColor = utils.randomColor(0)

martyButton = new Layer
	x: 10
	y: 90
	width: 70
	height: 50

martyButton.backgroundColor = utils.randomColor(0)

loucafeNext = new Layer
	x: 240
	y: 510
	width: 80
	height: 50

loucafeNext.backgroundColor = utils.randomColor(0)

loucafeButton = new Layer
	x: 10
	y: 160
	width: 70
	height: 50

loucafeButton.backgroundColor = utils.randomColor(0)

cafeNext = new Layer
	x: 240
	y: 510
	width: 80
	height: 50

cafeNext.backgroundColor = utils.randomColor(0)

cafeButton = new Layer
	x: 10
	y: 90
	width: 70
	height: 50

cafeButton.backgroundColor = utils.randomColor(0)

currLocButton = new Layer
	x: 20
	y: 475
	width: 80
	height: 50

currLocButton.backgroundColor = utils.randomColor(0)

HostButton = new Layer
	x: 20
	y: 475
	width: 80
	height: 50

HostButton.backgroundColor = utils.randomColor(0)

# Listen for clicks
HostButton.on Events.Click, ->
	imageLayer16.destroy()
	HostButton.destroy()
	
currLocButton.on Events.Click, ->
	imageLayer15.destroy()
	currLocButton.destroy()
	
cafeButton.on Events.Click, ->
	imageLayer14.destroy()
	cafeButton.destroy()
	
cafeNext.on Events.Click, ->
	imageLayer13.destroy()
	cafeNext.destroy()
	
loucafeButton.on Events.Click, ->
	imageLayer12.destroy()
	loucafeButton.destroy()
	
loucafeNext.on Events.Click, ->
	imageLayer11.destroy()
	loucafeNext.destroy()
	
martyButton.on Events.Click, ->
	imageLayer10.destroy()
	martyButton.destroy()
	
docButton.on Events.Click, ->
	imageLayer9.destroy()
	docButton.destroy()
	
martyDocNext.on Events.Click, ->
	imageLayer8.destroy()
	martyDocNext.destroy()
	
timeButton.on Events.Click, ->
	imageLayer7.destroy()
	timeButton.destroy()
	
timeNext.on Events.Click, ->
	imageLayer6.destroy()
	timeNext.destroy()
	
parksButton.on Events.Click, ->
	imageLayer5.destroy()
	parksButton.destroy()
	
cityButton.on Events.Click, ->
	imageLayer4.destroy()
	cityButton.destroy()
	
touristyButton.on Events.Click, ->
	imageLayer3.destroy()
	touristyButton.destroy()
	
featureNext.on Events.Click, ->
	imageLayer2.destroy()
	featureNext.destroy()
	
verifyButton.on Events.Click, ->
	imageLayer1.destroy()
	verifyButton.destroy()

	

